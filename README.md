# Protocole d'Utilisation - CDI Entry Logger

Le protocole d'utilisation suivant détaille les étapes à suivre pour utiliser efficacement l'application CDI Entry Logger.

## 1. Installation de l'Application

Suivez les instructions du document technique fourni pour pouvoir exécuter l’application CDI Entry Logger sur votre système.

## 2. Exécution de l'Application

Après avoir installé l'application, lancez-la en exécutant le script principal `application.pyw` à l'aide de Python. Assurez-vous que toutes les dépendances requises sont installées correctement sur votre système.

## 3. Interface Utilisateur

À l'ouverture de l'application, vous serez présenté avec une interface utilisateur. Elle comporte deux champs de texte : celui du bas est pour le code-barres, celui au-dessus est pour le code du mode professeur. En entrant le code `1234`, vous pouvez accéder à ce mode qui comporte plusieurs boutons :
- **Annuler** : ce bouton permet de revenir en mode élève.
- **Stats** : cette fonction permet d’appeler la fonction stats.
- **Fichier** : ce bouton permet de changer de base de données sans avoir à relancer l’application.
- Les deux champs de texte (NOM, Prénom) permettent de chercher une personne à partir de son nom et prénom.
- Le grand tableau au-dessus des champs de texte contient les personnes qui se sont enregistrées pendant que le programme tourne.

## 4. Enregistrement des Entrées des Élèves

Pour enregistrer l'entrée d'un élève, suivez les étapes suivantes :
- Saisissez le numéro de la carte d'identification de l'élève dans le champ prévu à cet effet.
- Les détails de l'élève seront automatiquement enregistrés dans le système, et une confirmation de l'enregistrement sera affichée à l'écran.
- Si vous n'avez pas de numéro mais un nom, entrez le code dans le champ prévu à cet effet.
- Appuyez sur valider pour accéder au mode professeur.
- Entrez le nom et le prénom de l’élève cherché et appuyez sur valider.
- Les détails de l'élève seront automatiquement enregistrés dans le système, et une confirmation de l'enregistrement sera affichée à l'écran. Si l’élève n'existe pas, on vous demandera de confirmer son ajout.

## 5. Consultation des Statistiques

L'application offre la possibilité de générer des statistiques à partir des données enregistrées. Pour consulter les statistiques, suivez ces étapes :
- Sélectionnez l'option de génération de statistiques dans le menu de l'application.
- Choisissez le mois pour lequel vous souhaitez générer les statistiques.
- Les statistiques seront générées et enregistrées dans un fichier dans le dossier `stats`, vous permettant d'analyser les tendances de fréquentation.

## 6. Gestion des Erreurs

En cas d'erreurs ou de problèmes rencontrés lors de l'utilisation de l'application, référez-vous aux messages d'erreur affichés à l'écran ou aux fichiers journaux générés par l'application. Ces informations vous aideront à diagnostiquer et à résoudre les problèmes éventuels.
