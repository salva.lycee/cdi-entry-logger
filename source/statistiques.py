from datetime import datetime

import pandas as pd

def find_weekday(date):
    # Convertir la chaîne de date en objet datetime
    date_string = date.strftime("%d/%m/%Y")
    date = datetime.strptime(date_string, "%d/%m/%Y")

    # Liste des jours de la semaine en français
    weekdays = ["Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"]

    # Obtenir le numéro du jour de la semaine (0 pour Lundi, 1 pour Mardi, etc.)
    day_number = date.weekday()

    # Retourner le nom du jour correspondant
    return weekdays[day_number]

def get_month_name(date):
    month_dict = {
        1: "Janvier",
        2: "Février",
        3: "Mars",
        4: "Avril",
        5: "Mai",
        6: "Juin",
        7: "Juillet",
        8: "Août",
        9: "Septembre",
        10: "Octobre",
        11: "Novembre",
        12: "Décembre"
    }
    date_string = date.strftime("%d/%m/%Y")
    date_object = datetime.strptime(date_string, "%d/%m/%Y")
    return month_dict[date_object.month]

def main(input_file, output_file, month):
    
    df = pd.read_excel(input_file)

    df["Date"] = pd.to_datetime(df["DATE"].astype("str"), format='%d/%m/%Y')
    df["Heure"] = pd.to_datetime(df["HEURE"].astype("str"), format='%H:%M')

    df["Jour"] = df["Date"].apply(find_weekday)
    df["Mois"] = df["Date"].apply(get_month_name)

    df["Date"] = df["Date"].dt.strftime("%d/%m/%Y")
    df["Heure"] = df["Heure"].dt.strftime("%Hh")

    dataframe = df[df["Mois"] == month]
    
    create_stats(dataframe, output_file)

def create_stats(df, output_file=''):
    """
    Crée des statistiques à partir d'un fichier Excel en utilisant pandas et datetime.

    Parameters:
    - input_file (str): Le chemin du fichier Excel en entrée.
    - output_file (str): Le nom du fichier Excel de sortie.

    Returns:
    Aucun (écrit les statistiques dans un fichier Excel).
    """

    jour = df.groupby(["Mois", "Jour", "Date"])["NOM"].count().reset_index(name="Nombre d'élèves")
    heure = df.groupby(["Mois", "Jour", "Date", "Heure"])["NOM"].count().reset_index(name="Nombre d'élèves")
    division = df.groupby(["Mois", "Jour", "Heure", "DIVISION"])["NOM"].count().reset_index(name="Nombre d'élèves")

    lundis = jour[jour['Jour'] == 'Lundi']
    mardis = jour[jour['Jour'] == 'Mardi']
    mercredis = jour[jour['Jour'] == 'Mercredi']
    jeudis = jour[jour['Jour'] == 'Jeudi']
    vendredis = jour[jour['Jour'] == 'Vendredi']

    lundis = lundis.drop(['Mois', 'Jour'], axis=1)
    mardis = mardis.drop(['Mois', 'Jour'], axis=1)
    mercredis = mercredis.drop(['Mois', 'Jour'], axis=1)
    jeudis = jeudis.drop(['Mois', 'Jour'], axis=1)
    vendredis = vendredis.drop(['Mois', 'Jour'], axis=1)

    heure_lundi = heure[heure['Jour'] == 'Lundi']
    heure_mardi = heure[heure['Jour'] == 'Mardi']
    heure_mercredi = heure[heure['Jour'] == 'Mercredi']
    heure_jeudi = heure[heure['Jour'] == 'Jeudi']
    heure_vendredi = heure[heure['Jour'] == 'Vendredi']

    heure_lundi = heure_lundi.drop(['Mois', 'Jour'], axis=1)
    heure_mardi = heure_mardi.drop(['Mois', 'Jour'], axis=1)
    heure_mercredi = heure_mercredi.drop(['Mois', 'Jour'], axis=1)
    heure_jeudi = heure_jeudi.drop(['Mois', 'Jour'], axis=1)
    heure_vendredi = heure_vendredi.drop(['Mois', 'Jour'], axis=1)

    moy_heure_lundi = heure_lundi.groupby(["Heure"])["Nombre d'élèves"].mean().reset_index()
    moy_heure_mardi = heure_mardi.groupby(["Heure"])["Nombre d'élèves"].mean().reset_index()
    moy_heure_mercredi = heure_mercredi.groupby(["Heure"])["Nombre d'élèves"].mean().reset_index()
    moy_heure_jeudi = heure_jeudi.groupby(["Heure"])["Nombre d'élèves"].mean().reset_index()
    moy_heure_vendredi = heure_vendredi.groupby(["Heure"])["Nombre d'élèves"].mean().reset_index()

    moy_divi_lundi = division[division['Jour'] == 'Lundi']
    moy_divi_mardi = division[division['Jour'] == 'Mardi']
    moy_divi_mercredi = division[division['Jour'] == 'Mercredi']
    moy_divi_jeudi = division[division['Jour'] == 'Jeudi']
    moy_divi_vendredi = division[division['Jour'] == 'Vendredi']

    moy_divi_lundi = moy_divi_lundi.drop(['Mois', 'Jour'], axis=1)
    moy_divi_mardi = moy_divi_mardi.drop(['Mois', 'Jour'], axis=1)
    moy_divi_mercredi = moy_divi_mercredi.drop(['Mois', 'Jour'], axis=1)
    moy_divi_jeudi = moy_divi_jeudi.drop(['Mois', 'Jour'], axis=1)
    moy_divi_vendredi = moy_divi_vendredi.drop(['Mois', 'Jour'], axis=1)

    moy_divi_lundi = moy_divi_lundi.set_index("Heure", inplace=False)
    moy_divi_mardi = moy_divi_mardi.set_index("Heure", inplace=False)
    moy_divi_mercredi = moy_divi_mercredi.set_index("Heure", inplace=False)
    moy_divi_jeudi = moy_divi_jeudi.set_index("Heure", inplace=False)
    moy_divi_vendredi = moy_divi_vendredi.set_index("Heure", inplace=False)

    moyenne_lundis = lundis["Nombre d'élèves"].mean()
    moyenne_mardis = mardis["Nombre d'élèves"].mean()
    moyenne_mercredis = mercredis["Nombre d'élèves"].mean()
    moyenne_jeudis = jeudis["Nombre d'élèves"].mean()
    moyenne_vendredis = vendredis["Nombre d'élèves"].mean()

    total_lundis = lundis["Nombre d'élèves"].sum()
    total_mardis = mardis["Nombre d'élèves"].sum()
    total_mercredis = mercredis["Nombre d'élèves"].sum()
    total_jeudis = jeudis["Nombre d'élèves"].sum()
    total_vendredis = vendredis["Nombre d'élèves"].sum()

    with pd.ExcelWriter(f"stat\\{output_file}") as writer:
        lundis.to_excel(writer, sheet_name='Lundi', startrow=2, index=False)
        mardis.to_excel(writer, sheet_name='Mardi', startrow=2, index=False)
        mercredis.to_excel(writer, sheet_name='Mercredi', startrow=2, index=False)
        jeudis.to_excel(writer, sheet_name='Jeudi', startrow=2, index=False)
        vendredis.to_excel(writer, sheet_name='Vendredi', startrow=2, index=False)

        moy_heure_lundi.to_excel(writer, sheet_name='Lundi', startrow=2, startcol=3, index=False)
        moy_heure_mardi.to_excel(writer, sheet_name='Mardi', startrow=2, startcol=3, index=False)
        moy_heure_mercredi.to_excel(writer, sheet_name='Mercredi', startrow=2, startcol=3, index=False)
        moy_heure_jeudi.to_excel(writer, sheet_name='Jeudi', startrow=2, startcol=3, index=False)
        moy_heure_vendredi.to_excel(writer, sheet_name='Vendredi', startrow=2, startcol=3, index=False)

        moy_divi_lundi.to_excel(writer, sheet_name='Lundi', startrow=2, startcol=6, index=True)
        moy_divi_mardi.to_excel(writer, sheet_name='Mardi', startrow=2, startcol=6, index=True)
        moy_divi_mercredi.to_excel(writer, sheet_name='Mercredi', startrow=2, startcol=6, index=True)
        moy_divi_jeudi.to_excel(writer, sheet_name='Jeudi', startrow=2, startcol=6, index=True)
        moy_divi_vendredi.to_excel(writer, sheet_name='Vendredi', startrow=2, startcol=6, index=True)

        workbook = writer.book
        worksheet_lundi = writer.sheets['Lundi']
        worksheet_mardi = writer.sheets['Mardi']
        worksheet_mercredi = writer.sheets['Mercredi']
        worksheet_jeudi = writer.sheets['Jeudi']
        worksheet_vendredi = writer.sheets['Vendredi']
        header_format = workbook.add_format({'bold': True, 'text_wrap': True})

        # Ajout des titres "Moyenne/Jours" et "Total"
        worksheet_lundi.write('A1', 'Moyenne', header_format)
        worksheet_lundi.write('B1', moyenne_lundis)
        worksheet_lundi.write('A2', 'Total', header_format)
        worksheet_lundi.write('B2', total_lundis)

        worksheet_mardi.write('A1', 'Moyenne', header_format)
        worksheet_mardi.write('B1', moyenne_mardis)
        worksheet_mardi.write('A2', 'Total', header_format)
        worksheet_mardi.write('B2', total_mardis)

        worksheet_mercredi.write('A1', 'Moyenne', header_format)
        worksheet_mercredi.write('B1', moyenne_mercredis)
        worksheet_mercredi.write('A2', 'Total', header_format)
        worksheet_mercredi.write('B2', total_mercredis)

        worksheet_jeudi.write('A1', 'Moyenne', header_format)
        worksheet_jeudi.write('B1', moyenne_jeudis)
        worksheet_jeudi.write('A2', 'Total', header_format)
        worksheet_jeudi.write('B2', total_jeudis)

        worksheet_vendredi.write('A1', 'Moyenne', header_format)
        worksheet_vendredi.write('B1', moyenne_vendredis)
        worksheet_vendredi.write('A2', 'Total', header_format)
        worksheet_vendredi.write('B2', total_vendredis)