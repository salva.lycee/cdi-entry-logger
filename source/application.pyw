# -*- coding: utf-8 -*-
# Nom du Programme : CDI-Entry-Logger
# Auteur : BARROT Antoine
# Description : Ce programme réalise un enregistrement des étudiants qui scanne leur carte.
# Licence : MIT
# Version : 3.0
# Date de création : 2023-10-10

# Instructions d'utilisation : Assurez-vous d'avoir installé les bibliothèques nécessaires en exécutant `pip install -r requirements.txt`.

# Imports
from PyQt5 import QtCore, QtGui, QtWidgets, QtMultimedia
from statistiques import get_month_name, main
import pandas as pd
import logging as log
import chardet
import datetime
import glob
import gzip
import sys
import os

now = datetime.date.today()
date = now.strftime("%d-%m-%Y")

log_directory = "log"
if not os.path.exists(log_directory):
    os.makedirs(log_directory)

log_filename = f'log_{date}.log'
chemin_fichier = os.path.join(log_directory, log_filename)

log.basicConfig(
    filename=chemin_fichier,
    format="%(asctime)s - %(levelname)s - %(message)s",
    level=log.NOTSET,
    encoding="utf-8"
)

log.log(log.INFO, "..." * 50) if os.path.exists(chemin_fichier) else None

# Classe principale de l'interface utilisateur (UI)
class Ui_MainWindow(object):
    # Initialisation et configuration de l'interface
    def setupUi(self, MainWindow):
        """
        This method sets up the user interface (UI) for the main window.
        It configures various widgets, layouts, and connections.
        """
        self.file_name = None

        # Compress log files
        self.compress_logs()

        log.debug("Creating the main window")
        # Main window configuration
        MainWindow.setObjectName("MainWindow")
        self.scanned_barcodes = {}
        self.exit_code = False
        self.MainWindow = MainWindow
        self.MainWindow.setWindowFlags(QtCore.Qt.WindowStaysOnTopHint)
        self.MainWindow.setWindowFlag(QtCore.Qt.FramelessWindowHint, True)
        self.MainWindow.showMaximized()
        self.MainWindow.show()
        self.MainWindow.raise_()

        # Custom central widget configuration
        self.centralWidget = QtWidgets.QWidget(MainWindow)
        self.centralWidget.setObjectName("centralWidget")
        MainWindow.setCentralWidget(self.centralWidget)

        # Create a vertical layout
        self.verticalLayout = QtWidgets.QVBoxLayout(self.centralWidget)
        self.verticalLayout.setObjectName("verticalLayout")

        # Create a horizontal layout
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")

        # Create a label to display the time
        self.dateTimeLabel = QtWidgets.QLabel(self.centralWidget)
        self.horizontalLayout.addWidget(self.dateTimeLabel, 0, QtCore.Qt.AlignRight)
        self.dateTimeLabel.setObjectName("dateTimeLabel")
        self.horizontalLayout.addWidget(self.dateTimeLabel)

        # Add the horizontal layout to the vertical layout
        self.verticalLayout.addLayout(self.horizontalLayout)

        self.label = QtWidgets.QLabel(self.centralWidget)

        # Center-align the text
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.verticalLayout.addWidget(self.label)

        # Create a table to display data
        self.tableWidget = QtWidgets.QTableWidget(self.centralWidget)
        self.tableWidget.viewport().setProperty("cursor", QtGui.QCursor(QtCore.Qt.ArrowCursor))
        self.tableWidget.setColumnCount(5)
        self.tableWidget.setObjectName("tableWidget")
        self.tableWidget.setRowCount(0)

        # Configure the horizontal header items of QTableWidget
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(0, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(1, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(2, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(3, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(4, item)

        # Configure the horizontal header column of QTableWidget
        # Set the default width of each header column.
        self.tableWidget.horizontalHeader().setDefaultSectionSize(125)
        # Set the minimum size of each header column.
        self.tableWidget.horizontalHeader().setMinimumSectionSize(32)
        self.tableWidget.hide()
        # Add the table widget to the vertical layout
        self.verticalLayout.addWidget(self.tableWidget)

        # Configure a horizontal layout (QHBoxLayout)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.verticalLayout.addLayout(self.horizontalLayout_2)

        # Create a horizontal layout (QHBoxLayout) and add it to another horizontal layout
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.horizontalLayout_2.addLayout(self.horizontalLayout_3)

        # Create and add a single-line text field (QLineEdit) to a horizontal layout
        self.nom = QtWidgets.QLineEdit(self.centralWidget)
        self.nom.setObjectName("lineEdit_nom")
        self.nom.hide()
        self.horizontalLayout_3.addWidget(self.nom)

        # Create a horizontal layout (QHBoxLayout) and add it to another horizontal layout
        self.horizontalLayout_4 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        self.horizontalLayout_2.addLayout(self.horizontalLayout_4)

        # Create and add a single-line text field (QLineEdit) to a horizontal layout
        self.prenom = QtWidgets.QLineEdit(self.centralWidget)
        self.prenom.setObjectName("lineEdit_prenom")
        self.prenom.hide()
        self.horizontalLayout_4.addWidget(self.prenom)

        # Create a horizontal layout (QHBoxLayout) and add it to another horizontal layout
        self.horizontalLayout_6 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_6.setObjectName("horizontalLayout_6")
        self.horizontalLayout_2.addLayout(self.horizontalLayout_6)

        # Create and add a button (QPushButton) to a horizontal layout
        self.pushButton = QtWidgets.QPushButton(self.centralWidget)
        self.pushButton.setObjectName("pushButton_valider")
        self.pushButton.hide()
        self.horizontalLayout_6.addWidget(self.pushButton)

        # Create a horizontal layout (QHBoxLayout) and add it to another horizontal layout
        self.horizontalLayout_7 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_7.setObjectName("horizontalLayout_7")
        self.verticalLayout.addLayout(self.horizontalLayout_7)

        # Create and add a button (QPushButton) to a horizontal layout
        self.pushButton_annuler = QtWidgets.QPushButton(self.centralWidget)
        self.pushButton_annuler.setObjectName("pushButton_annuler")
        self.pushButton_annuler.hide()
        self.horizontalLayout_7.addWidget(self.pushButton_annuler, 0, QtCore.Qt.AlignLeft)

        self.pushButton_stat = QtWidgets.QPushButton(self.centralWidget)
        self.pushButton_stat.setObjectName("pushButton_stat")
        self.pushButton_stat.hide()
        self.horizontalLayout_7.addWidget(self.pushButton_stat, 0, QtCore.Qt.AlignLeft)

        self.pushButton_fichier = QtWidgets.QPushButton(self.centralWidget)
        self.pushButton_fichier.setObjectName("pushButton_fichier")
        self.pushButton_fichier.hide()
        self.horizontalLayout_7.addWidget(self.pushButton_fichier, 0, QtCore.Qt.AlignLeft)

        # Create a text field to enter the code sequence
        self.code = QtWidgets.QLineEdit(self.centralWidget)
        self.code.setObjectName("code")
        self.code.setEchoMode(QtWidgets.QLineEdit.Password)
        self.horizontalLayout_7.addWidget(self.code, 0, QtCore.Qt.AlignRight)

        # Create and add a button (QPushButton) to a horizontal layout
        self.pushButton_code = QtWidgets.QPushButton(self.centralWidget)
        self.pushButton_code.setObjectName("pushButton_code")
        self.horizontalLayout_7.addWidget(self.pushButton_code, 0, QtCore.Qt.AlignLeft)

        # Create a text field to enter the barcode sequence
        self.scanner_input = QtWidgets.QLineEdit(self.centralWidget)
        self.scanner_input.setObjectName("scanner_input")
        self.scanner_input.setFocus()
        self.verticalLayout.addWidget(self.scanner_input)

        # Connect the input field to the automatic search function
        self.scanner_input.textChanged.connect(self.search_information)

        log.debug("Opening files")
        self.ouverture_base()

        if not os.path.exists("Base_Temp.xlsx"):
            self.base_uid = pd.DataFrame(columns=["NOM", "PRENOM", "DIVISION", "UID"])
            self.base_uid.to_excel("Base_Temp.xlsx", index=False)
            log.warning("File 'Base_Temp.xlsx' not found, creating another")
        else:
            self.base_uid = pd.read_excel("Base_Temp.xlsx")
            log.debug("File 'Base_Temp.xlsx' loaded.")

        if not os.path.exists("Entree_CDI.xlsx"):
            self.df = pd.DataFrame(columns=["DATE", "HEURE", "NOM", "PRENOM", "DIVISION", "BADGE"])
            self.df.to_excel("Entree_CDI.xlsx", index=False)
            log.warning("File 'Entree_CDI.xlsx' not found, creating another")
        else:
            self.df = pd.read_excel("Entree_CDI.xlsx")
            log.debug("File 'Entree_CDI.xlsx' loaded.")

        # Configure translation of the user interface (UI) and signal connections
        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

        # Start a QTimer to update the time every second
        self.timer = QtCore.QTimer(MainWindow)
        self.timer.timeout.connect(self.update_time)
        self.timer.start(1000)

        self.current_date = datetime.datetime.now().date()

        self.timer = QtCore.QTimer(MainWindow)
        self.timer.timeout.connect(self.update_time)
        self.timer.start(1000)
    def retranslateUi(self, MainWindow):
        """
        This method is responsible for translating the user interface components.
        It sets the text and placeholders for various elements in the UI.
        """
        _translate = QtCore.QCoreApplication.translate

        # Set the text for the dateTimeLabel
        self.dateTimeLabel.setText(_translate("MainWindow", "Date et Heure:"))

        # Set the text for the horizontal header items in the tableWidget
        item = self.tableWidget.horizontalHeaderItem(0)
        item.setText(_translate("MainWindow", "Date et Heure"))
        item = self.tableWidget.horizontalHeaderItem(1)
        item.setText(_translate("MainWindow", "NOM"))
        item = self.tableWidget.horizontalHeaderItem(2)
        item.setText(_translate("MainWindow", "Prenom"))
        item = self.tableWidget.horizontalHeaderItem(3)
        item.setText(_translate("MainWindow", "Classe"))
        item = self.tableWidget.horizontalHeaderItem(4)
        item.setText(_translate("MainWindow", "Badge"))

        # Set the placeholder text for the nom QLineEdit
        self.nom.setPlaceholderText(_translate("MainWindow", "NOM"))
        self.nom.returnPressed.connect(self.validate_clicked)
        self.nom.textChanged.connect(self.convert_to_upper_case)

        # Set the placeholder text for the prenom QLineEdit
        self.prenom.setPlaceholderText(_translate("MainWindow", "Prenom"))
        self.prenom.returnPressed.connect(self.validate_clicked)

        # Set the placeholder text for the code QLineEdit
        self.code.setPlaceholderText(_translate("MainWindow", "Entrer le code"))
        self.code.returnPressed.connect(self.validate_code)
        self.code.returnPressed.connect(self.replace_special_characters)

        # Set the text for the Valider pushButton and connect its click event
        self.pushButton.setText(_translate("MainWindow", "Valider"))
        self.pushButton.clicked.connect(self.validate_clicked)

        # Set the text for the Valider pushButton_code and connect its click event
        self.pushButton_code.setText(_translate("MainWindow", "Valider"))
        self.pushButton_code.clicked.connect(self.validate_code)

        # Set the text for the Annuler pushButton_annuler and connect its click event
        self.pushButton_annuler.setText(_translate("MainWindow", "Annuler"))
        self.pushButton_annuler.clicked.connect(self.deactivate_input)

        # Set the text for the Stat pushButton_stat and connect its click event
        self.pushButton_stat.setText(_translate("MainWindow", "Stats"))
        self.pushButton_stat.clicked.connect(self.stats)

        # Set the text for the Fichier pushButton_fichier and connect its click event
        self.pushButton_fichier.setText(_translate("MainWindow", "Fichier"))
        self.pushButton_fichier.clicked.connect(self.open_file_dialog)

        # Connect the textChanged signal of scanner_input to replace_special_characters method
        self.scanner_input.textChanged.connect(self.replace_special_characters)
    def switch_to_student_mode(self):
        """
        Switches the display to student mode by showing the label and hiding the table widget.
        """
        self.label.show()
        self.tableWidget.hide()
    def switch_to_prof_mode(self):
        """
        Switches the display to professor mode by hiding the label and showing the table widget.
        """
        self.label.hide()
        self.tableWidget.show()
    def input_dialog(self):
        dialog = QtWidgets.QDialog()

        layout = QtWidgets.QFormLayout(dialog)

        nameEdit = QtWidgets.QLineEdit(dialog)
        nameCompleter = QtWidgets.QCompleter(self.data["NOM"], nameEdit)
        nameEdit.setCompleter(nameCompleter)
        layout.addRow("NOM", nameEdit)

        surnameEdit = QtWidgets.QLineEdit(dialog)
        surnameCompleter = QtWidgets.QCompleter(self.data["PRENOM"], surnameEdit)
        surnameEdit.setCompleter(surnameCompleter)
        layout.addRow("PRENOM", surnameEdit)

        divisionEdit = QtWidgets.QLineEdit(dialog)
        divisionCompleter = QtWidgets.QCompleter(self.data["DIVISION"], divisionEdit)
        divisionEdit.setCompleter(divisionCompleter)
        layout.addRow("DIVISION", divisionEdit)

        buttonBox = QtWidgets.QDialogButtonBox(QtWidgets.QDialogButtonBox.Ok | QtWidgets.QDialogButtonBox.Cancel, QtCore.Qt.Horizontal, dialog)
        buttonBox.accepted.connect(dialog.accept)
        buttonBox.rejected.connect(dialog.reject)
        layout.addRow(buttonBox)

        if dialog.exec_():
            return nameEdit.text(), surnameEdit.text(), divisionEdit.text()

    # Gestion des événements
    def closeEvent(self, event):
        """
        Event handler for the close event of the window.

        Args:
            event (QCloseEvent): The close event object.
        """
        log.critical("Shutdown attempt...")

        # Check if the exit code is present
        if not self.exit_code:
            # If exit code is not present, cancel the close event
            event.ignore()
            log.critical("Shutdown attempt canceled")
        else:
            # If exit code is present, allow the close event
            log.info("Shutdown authorized")
    def validate_clicked(self):
        """
        Handles the action when the "Validate" button is clicked.

        This method retrieves the entered name and surname, checks for empty fields,
        searches the database for the corresponding card number, and performs actions accordingly.
        """
        # Get the entered name and surname
        nom = self.nom.text()
        prenom = self.prenom.text()

        log.debug(f"Entered name and surname: {nom} {prenom}")

        # Check if any of the three fields is empty
        if not nom or not prenom:
            QtWidgets.QMessageBox.critical(
                MainWindow, "Erreur", "Tous les champs doivent être remplis"
            )
            log.warning(
                "Attempt to enter only one field to search for a person without a card"
            )
            return  # Do not validate if any field is empty

        # Get the current date and time
        now = QtCore.QDateTime.currentDateTime()
        date_heure = now.toString("dd/MM/yyyy HH:mm")
        date = now.toString("dd/MM/yyyy")
        heure = now.toString("HH:mm")

        # Search for the card number in the DataFrame self.data
        condition = (self.data["NOM"] == nom) & (self.data["PRENOM"] == prenom)

        if not self.data[condition].empty:
            if not self.data["BADGE"].empty:
                numero_carte_jeune = self.data[condition]["BADGE"].values[0]

            classe = self.data[condition]["DIVISION"].values[0]

            # If a card number is found, add the data to the table (QTableWidget)
            row_position = self.tableWidget.rowCount()
            self.tableWidget.insertRow(row_position)
            self.tableWidget.setItem(row_position, 0, QtWidgets.QTableWidgetItem(date_heure))
            self.tableWidget.setItem(row_position, 1, QtWidgets.QTableWidgetItem(nom))
            self.tableWidget.setItem(row_position, 2, QtWidgets.QTableWidgetItem(prenom))
            self.tableWidget.setItem(row_position, 3, QtWidgets.QTableWidgetItem(str(classe)))
            self.tableWidget.setItem(row_position, 4, QtWidgets.QTableWidgetItem(str(int(numero_carte_jeune))))

            # Clear the fields after validation
            self.nom.clear()
            self.prenom.clear()

            # Lock the fields after validation
            self.deactivate_input()

            # Export data to the file Entry_CDI.xlsx
            new_data = pd.DataFrame([[date, heure, nom, prenom, classe, numero_carte_jeune]], columns=["DATE", "HEURE", "NOM", "PRENOM", "DIVISION", "BADGE"])

            self.df = pd.concat([self.df, new_data], ignore_index=True)

            self.df.to_excel("Entree_CDI.xlsx", index=False)
            log.info(f"A person has been manually added: {nom} {prenom}")

            self.timer = QtCore.QTimer()
            self.timer.timeout.connect(self.hide_text)
            self.label.setStyleSheet("font-size: 30px; color: green;")
            self.label.setText(f"Hello, {prenom} {nom}")
            display_duration = 5000
            self.timer.start(display_duration)

        elif not self.data["NOM"].empty:
            prenom = self.replace_accents(prenom)

            condition = (self.data["NOM"] == nom) & (self.data["PRENOM"] == prenom)

            if not self.data[condition].empty:
                if not self.data["BADGE"].empty:
                    numero_carte_jeune = self.data[condition]["BADGE"].values[0]

                classe = self.data[condition]["DIVISION"].values[0]

                # If a card number is found, add the data to the table (QTableWidget)
                row_position = self.tableWidget.rowCount()
                self.tableWidget.insertRow(row_position)
                self.tableWidget.setItem(row_position, 0, QtWidgets.QTableWidgetItem(date_heure))
                self.tableWidget.setItem(row_position, 1, QtWidgets.QTableWidgetItem(nom))
                self.tableWidget.setItem(row_position, 2, QtWidgets.QTableWidgetItem(prenom))
                self.tableWidget.setItem(row_position, 3, QtWidgets.QTableWidgetItem(str(classe)))
                self.tableWidget.setItem(row_position, 4, QtWidgets.QTableWidgetItem(str(int(numero_carte_jeune))))

                # Clear the fields after validation
                self.nom.clear()
                self.prenom.clear()

                # Lock the fields after validation
                self.deactivate_input()

                # Export data to the file Entry_CDI.xlsx
                new_data = pd.DataFrame([[date, heure, nom, prenom, classe, numero_carte_jeune]], columns=["DATE", "HEURE", "NOM", "PRENOM", "DIVISION", "BADGE"])

                self.df = pd.concat([self.df, new_data], ignore_index=True)

                self.df.to_excel("Entry_CDI.xlsx", index=False)
                log.info(f"A person has been manually added: {nom} {prenom}")

                self.timer = QtCore.QTimer()
                self.timer.timeout.connect(self.hide_text)
                self.label.setStyleSheet("font-size: 30px; color: green;")
                self.label.setText(f"Hello, {prenom} {nom}")
                display_duration = 5000
                self.timer.start(display_duration)

        else:
            QtWidgets.QMessageBox.critical(MainWindow, "Erreur", "Aucun numéro de carte trouvé pour les informations fournies.")
            reply = QtWidgets.QMessageBox.question(MainWindow, "Question", "Voulez vous quand même l'ajouter ?", QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No, QtWidgets.QMessageBox.No)
            if reply == QtWidgets.QMessageBox.Yes:
                text, ok = QtWidgets.QInputDialog.getText(MainWindow, 'Classe', 'Veuillez entrer la classe :')
                row_position = self.tableWidget.rowCount()
                self.tableWidget.insertRow(row_position)
                self.tableWidget.setItem(row_position, 0, QtWidgets.QTableWidgetItem(date_heure))
                self.tableWidget.setItem(row_position, 1, QtWidgets.QTableWidgetItem(nom))
                self.tableWidget.setItem(row_position, 2, QtWidgets.QTableWidgetItem(prenom))
                if ok:
                    self.tableWidget.setItem(row_position, 3, QtWidgets.QTableWidgetItem(text))

            else:
                log.warning("Failed attempt to add a person manually: No card number found for the information")
            self.nom.clear()
            self.prenom.clear()
    def activate_input(self):
        """
        Enables input-related elements and switches to professor mode.

        This method shows and enables certain UI elements, sets flags,
        and switches the window to professor mode.
        """
        # Show and enable input-related elements
        self.pushButton_fichier.show()
        self.nom.show()
        self.prenom.show()
        self.pushButton.show()
        self.pushButton_annuler.show()
        self.pushButton_stat.show()

        # Set flags
        self.exit_code = True

        # Reset window flags and show the window
        self.MainWindow.setWindowFlag(QtCore.Qt.FramelessWindowHint, False)
        self.MainWindow.showMaximized()
        self.MainWindow.show()
        self.MainWindow.lower()

        # Switch to professor mode
        self.switch_to_prof_mode()

        log.debug("Input activation and switching to professor mode")
    def deactivate_input(self):
        """
        Disables input-related elements and switches to student mode.

        This method hides and disables certain UI elements, resets flags,
        and switches the window to student mode.
        """
        # Hide and disable input-related elements
        self.pushButton_fichier.hide()
        self.nom.hide()
        self.prenom.hide()
        self.pushButton.hide()
        self.pushButton_annuler.hide()
        self.pushButton_stat.hide()

        # Reset flags
        self.exit_code = False

        # Set window flags and focus on scanner input
        self.MainWindow.setWindowFlag(QtCore.Qt.FramelessWindowHint, True)
        self.MainWindow.show()
        self.MainWindow.raise_()
        self.scanner_input.setFocus()

        # Switch to student mode
        self.switch_to_student_mode()

        log.debug("Input deactivation and switching to student mode")

    # Gestion du temps et de la date
    def update_time(self):
        """
        This method will be called every second to update the displayed date and time.
        """
        # Get the current date and time
        now = datetime.datetime.now()

        # Format the current time as a string with the specified format
        current_time = now.strftime("%d/%m/%Y %H:%M:%S")

        # Update the text of the dateTimeLabel with the current date and time
        self.dateTimeLabel.setText("Date et Heure : " + current_time)
    def change_date(self):
        """
        Checks if the current date is different from the date stored in 'current_date'.
        If they are different, updates 'current_date' to the current date and calls 'close_log' and 'reopen_log'.
        """
        # Check if the current date is different from the stored date
        if current_date != datetime.now().date():
            # Update 'current_date' to the current date
            current_date = datetime.now().date()

            # Call the 'close_log' and 'reopen_log' method
            self.close_log()
            self.reopen_log()

    # Gestion des recherches
    def search_information(self):
        """
        Searches for information based on the entered barcode data.

        This method retrieves the barcode data entered in the text field, checks its validity,
        searches the database for information, and performs actions accordingly.
        """
        # Get the entered barcode data from the text field
        barcode_data = self.scanner_input.text()

        # Check if the barcode data is a valid length and consists of digits
        if len(barcode_data) == 8 and barcode_data.isdigit():
            barcode_data = barcode_data[:-1]

            # Check if the barcode has been scanned recently
            if barcode_data in self.scanned_barcodes:
                timestamp = self.scanned_barcodes[barcode_data]
                current_time = QtCore.QDateTime.currentDateTime()
                time_difference = timestamp.secsTo(current_time)

                # Display an error message if the barcode has been scanned recently
                if time_difference < 300:  # 300 seconds = 5 minutes
                    self.timer = QtCore.QTimer()
                    self.timer.timeout.connect(self.hide_text)
                    self.label.setStyleSheet("font-size: 100px; color: red;")
                    self.label.setText("Already Checked In")
                    display_duration = 5000
                    self.timer.start(display_duration)

                    self.play_sound("erreur.waw")
                    log.warning("Attempt to rescan a card before the 5-minute break")
                    self.scanner_input.clear()
                    return

            # Check the condition against the database to get information from the barcode_data
            condition = self.data["BADGE"] == int(barcode_data)

            # Query the database to get information from the barcode_data
            if not self.data[condition].empty:
                nom = str(self.data[condition]["NOM"].values[0])
                prenom = str(self.data[condition]["PRENOM"].values[0])
                classe = str(self.data[condition]["DIVISION"].values[0])
                numero_carte_jeune = barcode_data

                # Get the current date and time
                now = QtCore.QDateTime.currentDateTime()
                date_heure = now.toString("dd/MM/yyyy HH:mm")
                date = now.toString("dd/MM/yyyy")
                heure = now.toString("HH:mm")

                # Add information to the table (QtWidgets.QTableWidget)
                row_position = self.tableWidget.rowCount()

                self.tableWidget.insertRow(row_position)
                self.tableWidget.setItem(row_position, 0, QtWidgets.QTableWidgetItem(date_heure))
                self.tableWidget.setItem(row_position, 1, QtWidgets.QTableWidgetItem(nom))
                self.tableWidget.setItem(row_position, 2, QtWidgets.QTableWidgetItem(prenom))
                if classe == "nan":
                    classe = "Sans Classe"
                    self.tableWidget.setItem(row_position, 3, QtWidgets.QTableWidgetItem(classe))
                else:
                    self.tableWidget.setItem(row_position, 3, QtWidgets.QTableWidgetItem(classe))
                self.tableWidget.setItem(row_position, 4, QtWidgets.QTableWidgetItem(str(numero_carte_jeune)))

                # Display a greeting message
                self.timer = QtCore.QTimer()
                self.timer.timeout.connect(self.hide_text)
                self.label.setStyleSheet("font-size: 30px; color: green;")
                self.label.setText(f"Hello, {prenom} {nom}")
                display_duration = 5000
                self.timer.start(display_duration)

                # Add data to the Pandas DataFrame
                new_data = pd.DataFrame(
                    [[date, heure, nom, prenom, classe, numero_carte_jeune]],
                    columns=["DATE", "HEURE", "NOM", "PRENOM", "DIVISION", "BADGE"],
                )

                self.df = pd.concat([self.df, new_data], ignore_index=True)

                # Save the DataFrame to an Excel file
                self.df.to_excel("Entree_CDI.xlsx", index=False)
                log.info(f"A person has been recorded after entering their card number: {prenom} {nom}")

                # If the barcode is valid and has not been scanned recently, add it to the dictionary
                self.scanned_barcodes[barcode_data] = QtCore.QDateTime.currentDateTime()

                # Clear the input field after the search
                self.scanner_input.clear()

            else:
                # Display an error message for no matching person
                self.play_sound("erreur.waw")
                QtWidgets.QMessageBox.critical(MainWindow, "Error", "No person matches the provided barcode.")
                log.warning("Failed attempt to scan a card")
                self.scanner_input.clear()
        """
        log.debug("Tentative de carte")
        try:
            # Ouvrir la connexion série
            ser = serial.Serial('COM6', '9600', timeout='2')
            log.debug(ser)
            time.sleep(0.001)

            # Lire les données du moniteur série en permanence
            ligne = ser.readline().decode('utf-8').rstrip('\r\n')
            log.debug(ligne)
            if ligne:
                self.search_information_hexa(ligne)

        except Exception as e:
            print(f"Erreur : {e}")
        """
    def search_information_hexa(self, uid : str):
        """
        Searches for information based on the UID of the card

        This method retrieves the UID entered in the text field, checks its validity,
        searches the database for information, and performs actions accordingly.
        """
        log.debug(f"Uid converti en hexa {uid}")
        
        # Check the condition against the database to get information from the barcode_data
        condition = self.base_uid["UID"] == uid

        # Query the database to get information from the barcode_data
        if not self.base_uid[condition].empty:
            log.debug(f"UID Trouvée {condition}")
            nom = str(self.base_uid[condition]["NOM"].values[0])
            prenom = str(self.base_uid[condition]["PRENOM"].values[0])
            classe = str(self.base_uid[condition]["DIVISION"].values[0])

            # Get the current date and time
            now = QtCore.QDateTime.currentDateTime()
            date_heure = now.toString("dd/MM/yyyy HH:mm")
            date = now.toString("dd/MM/yyyy")
            heure = now.toString("HH:mm")

            # Add information to the table (QtWidgets.QTableWidget)
            row_position = self.tableWidget.rowCount()

            self.tableWidget.insertRow(row_position)
            self.tableWidget.setItem(row_position, 0, QtWidgets.QTableWidgetItem(date_heure))
            self.tableWidget.setItem(row_position, 1, QtWidgets.QTableWidgetItem(nom))
            self.tableWidget.setItem(row_position, 2, QtWidgets.QTableWidgetItem(prenom))
            if classe == "nan":
                classe = "Sans Classe"
                self.tableWidget.setItem(row_position, 3, QtWidgets.QTableWidgetItem(classe))
            else:
                self.tableWidget.setItem(row_position, 3, QtWidgets.QTableWidgetItem(classe))

            # Display a greeting message
            self.timer = QtCore.QTimer()
            self.timer.timeout.connect(self.hide_text)
            self.label.setStyleSheet("font-size: 30px; color: green;")
            self.label.setText(f"Hello, {prenom} {nom}")
            display_duration = 5000
            self.timer.start(display_duration)

            # Add data to the Pandas DataFrame
            new_data = pd.DataFrame(
                [[date, heure, nom, prenom, classe]],
                columns=["DATE", "HEURE", "NOM", "PRENOM", "DIVISION"],
            )

            self.df = pd.concat([self.df, new_data], ignore_index=True)

            # Save the DataFrame to an Excel file
            self.df.to_excel("Entree_CDI.xlsx", index=False)
            log.info(f"A person has been recorded after scanning their card: {prenom} {nom}")

        else:
            reply = QtWidgets.QMessageBox.question(MainWindow, "Nouvelle Carte Jeune", "Voulez ajouter cette carte ?", QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No, QtWidgets.QMessageBox.No)

            if reply:
                text, ok = QtWidgets.QInputDialog.getText(MainWindow, "Nouvelle Carte Jeune", "Veillez entrer le numéro de badge, si pas de numéro de carte cliquer sur non")

                if ok:
                    condition = self.data["BADGE"] == text

                    # Query the database to get information from the barcode_data
                    if not self.base_uid[condition].empty:
                        surname = str(self.base_uid[condition]["NOM"].values[0])
                        name = str(self.base_uid[condition]["PRENOM"].values[0])
                        division = str(self.base_uid[condition]["DIVISION"].values[0])
                else:
                    name, surname, division = self.input_dialog()
                    surname = self.input_box("NOM", "Entrez votre nom").upper()
                    name =  self.input_box("PRENOM", "Entrez votre prenom").capitalize()
                    division : self.input_box("CLASSE", "Entrez votre classe")

                new_user = {
                    "NOM" : surname.upper(),
                    "PRENOM" : name.capitalize(),
                    "DIVISION" : int(division),
                    "UID" : uid,
                }

                # Check if the user already exists in the database
                condition = (self.base_uid["NOM"] == new_user["NOM"]) & (self.base_uid["PRENOM"] == new_user["PRENOM"]) & (self.base_uid["DIVISION"] == new_user["DIVISION"])

                if self.base_uid[condition].empty:
                    # If the user does not exist, append the new user
                    self.base_uid = self.base_uid._append(new_user, ignore_index=True)
                else:
                    # If the user exists, ask if you want to replace it
                    ok = QtWidgets.QMessageBox.question(MainWindow, "Utilisateur existant", "Cet utilisateur existe déjà, voulez-vous le remplacer ?", QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No, QtWidgets.QMessageBox.No)

                    if ok:
                        # If you want to replace the user, remove the existing user and append the new user
                        self.base_uid = self.base_uid[~condition]
                        self.base_uid = self.base_uid._append(new_user, ignore_index=True)

                self.base_uid.to_excel('Base_Temp.xlsx', index=False)

    # Gestion des statistiques
    def stats(self):
        """
        Generates statistics based on input data.

        This method calls the 'create_stats' function with specific parameters
        to create and save statistics an Excel file.
        """
        month = self.combo_box(
            "Choisir le mois",
            "Veuiller sélectionner le mois",
            [
                "Janvier",
                "Février",
                "Mars",
                "Avril",
                "Mai",
                "Juin",
                "Juillet",
                "Août",
                "Septembre",
                "Octobre",
                "Novembre",
                "Décembre"
            ]
        )
        try:
            month = get_month_name(datetime.datetime.strptime("11/10/2023", "%d/%m/%Y"))
            main('Entree_CDI.xlsx', f"Stat_{month}.xlsx", month)
            log.debug(f"Fichier 'Stat_{month}.xlsx' créer")
        
        except Exception as e:
            log.error(e)

    # Gestion des codes
    def validate_code(self):
        """
      nan_inf_to_errors  Validates the entered code and takes appropriate actions.

        This method retrieves the entered code, checks its validity,
        and performs actions based on the code.
        """
        # Get the entered code from the input field
        code = self.code.text()

        # Check if the entered code consists of digits
        if code.isdigit() and code == "1234":
            # The code is valid, call activate_input to enable the name, surname, and class fields
            self.activate_input()
            log.warning("Correct code entered")

        else:
            # Display an error message for an invalid code
            QtWidgets.QMessageBox.critical(MainWindow, "Error", "Incorect code. Please enter a valid code.")
            log.critical("Failed code entry attempt: Incorect code")
            self.play_sound("erreur.waw")

        # Clear the code input field
        self.code.clear()
    def play_sound(self,audio_file):
        """
        Plays a sound effect.

        This method initializes a QSoundEffect, sets its source to an audio file,
        configures the volume, and plays the sound effect.
        """
        # Initialize QSoundEffect
        self.sound_effect = QtMultimedia.QSoundEffect()

        # Set the source of the sound effect (replace "erreur.wav" with your audio file path)
        self.sound_effect.setSource(QtCore.QUrl.fromLocalFile(audio_file))

        # Set the volume of the sound effect
        self.sound_effect.setVolume(10.0)

        # Play the sound effect
        self.sound_effect.play()

    # Gestion des logs
    def close_log(self):
        """
        Closes the log file and shuts down the logging system.
        """
        log.info("Closing the log")
        log.shutdown()
    def reopen_log(self):
        """
        Reopens the log file with a new filename based on the current date.
        """
        now = datetime.date.today()
        date_str = now.strftime("%d-%m-%Y")
        log_filename = f'log_{date_str}.log'
        chemin_fichier = os.path.join('log', log_filename)

        # Check if the directory exists, otherwise, create it
        log_dir = os.path.dirname(chemin_fichier)
        if not os.path.exists(log_dir):
            os.makedirs(log_dir)

        # Reset the log configuration with the new file
        log.basicConfig(
            filename=chemin_fichier,
            format="%(asctime)s - %(levelname)s - %(message)s",
            level=log.NOTSET,
            encoding="utf-8"
        )

        log.info("Reopening the log")
    def compress_logs(self):
        """
        Compresses the most recent log file in the 'log' directory and deletes the original file.
        Does not compress the log file of the current day.

        Raises:
            IndexError: If there are no log files or an error occurs during compression.
        """
        try:
            # Get today's date
            today = datetime.date.today()

            # Search for all existing log files
            log_files = glob.glob(os.path.join('log', 'log_*.log'))

            # Sort the files by date, from most recent to oldest
            log_files.sort(key=os.path.getctime, reverse=True)

            # Check if there is at least one log file
            if log_files:
                try:
                    # Iterate over the sorted log files
                    for log_file in log_files:
                        # Extract the date from the log file name
                        log_date_str = os.path.basename(log_file).split('_')[1].split('.')[0]
                        log_date = datetime.datetime.strptime(log_date_str, "%d-%m-%Y").date()

                        # Check if the log file is from today
                        if log_date != today:
                            # Create the name of the compressed file
                            compressed_filename = log_file + '.gz'

                            # Open the original log file in binary read mode
                            with open(log_file, 'rb') as original_file:
                                # Open the compressed file in binary write mode
                                with gzip.open(compressed_filename, 'wb') as compressed_file:
                                    # Copy the data from the original file to the compressed file
                                    compressed_file.writelines(original_file)

                            log.debug(f'Le fichier journal {log_file} a été compressé en {compressed_filename}')

                            # Delete the original log file
                            try:
                                os.remove(log_file)
                                log.debug(f'Le fichier journal {log_file} a été supprimé.')
                            except PermissionError as e:
                                log.error(f'Erreur lors de la suppression du fichier journal : {e}')
                            # Exit the loop after compressing the most recent log file that is not from today
                            break

                except IndexError:
                    # Handle the error if the index is out of range
                    log.debug('Aucun fichier journal trouvé ou erreur lors de la compression.')

            else:
                log.debug('Aucun fichier journal trouvé.')

        except Exception as e:
            log.error(f"Une erreur inattendue s'est produite : {str(e)}")

    # Gestion des fichiers
    def open_file_dialog(self):
        """
        Opens a file dialog to select a CSV file, detects its encoding, and reencodes it to UTF-8.

        Raises:
            chardet.UniversaldetectorError: If there is an error in encoding detection.
            Exception: For other unexpected errors.
        """
        try:
            options = QtWidgets.QFileDialog.Options()
            options |= QtWidgets.QFileDialog.ReadOnly  # Allow only read access
            file_dialog = QtWidgets.QFileDialog()
            file_name, _ = file_dialog.getOpenFileName(
                MainWindow, "Ouvrir un fichier", "",
            )

            if file_name:
                log.debug("Fichier sélectionné : " + file_name)
                self.file_name = file_name
                detected_encoding = self.detect_file_encoding(file_name)
                rencod = self.read_csv_with_encoding(file_name, detected_encoding)

                # Perform data transformations and save the DataFrame to the same CSV file
                self.process_and_save_dataframe(rencod)

                log.debug("Réouverture de 'base_orcadia.csv'")
                self.ouverture_base()

        except chardet.UniversaldetectorError as e:
            log.error(f"Erreur de détection de l'encodage : {str(e)}")
        except Exception as e:
            log.error(f"Une erreur inattendue s'est produite : {str(e)}")
    def detect_file_encoding(self, file_name):
        """
        Detects the encoding of a file using the chardet library.

        Args:
            file_name (str): The name of the file.

        Returns:
            str: The detected encoding.
        """
        with open(file_name, "rb") as file:
            result = chardet.detect(file.read())
            log.debug(result)
            detected_encoding = result["encoding"]
            log.debug(detected_encoding)
        return detected_encoding
    def read_csv_with_encoding(self, file_name, encoding):
        """
        Reads a CSV file using the specified encoding.

        Args:
            file_name (str): The name of the file.
            encoding (str): The encoding to use.

        Returns:
            pd.DataFrame: The DataFrame read from the CSV file.
        """
        delimiter = self.detect_delimiter(file_name, encoding)
        return pd.read_csv(file_name, encoding=encoding, delimiter=delimiter)
    def process_and_save_dataframe(self, dataframe):
        """
        Processes the DataFrame and saves it to the same CSV file.

        Args:
            dataframe (pd.DataFrame): The DataFrame to process and save.
        """
        try:
            col_names = ["NOM", "PRENOM", "DIVISION", "BADGE"]
            dataframe.columns = col_names

            dataframe['NOM'] = dataframe['NOM'].str.replace('PRO ', '')
            dataframe['NOM'] = dataframe['NOM'].str.replace('POST ', '')

            dataframe['NOM'] = dataframe['NOM'].str.upper()
            dataframe['PRENOM'] = dataframe['PRENOM'].str.capitalize()

            dataframe.to_csv("base_orcadia.csv", encoding="UTF-8", index=False)
            log.debug("Fichier 'base_orcadia.csv' réencodée en UTF-8")
            log.debug("Fichier 'base_orcadia.csv' réécrit dans le dossier")

        except Exception as e:
            log.error(str(e))
    def detect_delimiter(self, file_name, encoding):
        """
        Detects the delimiter used in a CSV file.

        :param file_name: The name of the CSV file.
        :param encoding: The encoding of the CSV file.
        :return: The detected delimiter, or ';' (comma) as default if none is found.
        """
        delimiters = [',', ';', '\t']  # Add more delimiters if necessary

        with open(file_name, 'r', encoding=encoding) as file:
            for line in file:
                for delimiter in delimiters:
                    if delimiter in line:
                        return delimiter
        return ';'  # If no delimiter is found, use comma as the default
    def ouverture_base(self):
        """
        Opens the 'base_orcadia.csv' file and loads its content into a Pandas DataFrame.
        
        If the file is not found, displays an error message and creates an empty DataFrame.
        """
        try:
            self.data = pd.read_csv("base_orcadia.csv", encoding="utf-8")
            QtWidgets.QMessageBox.information(MainWindow, "Succès", "Fichier 'base_orcadia.csv' chargé avec succès.")
            log.debug("Fichier 'base_orcadia' chargé")

        except FileNotFoundError:
            # If the file is not found, create an empty DataFrame
            QtWidgets.QMessageBox.critical(MainWindow, "Erreur", "Fichier 'base_orcadia.csv' introuvable.")
            log.error("Impossible de trouver le fichier 'base_orcadia.csv'")

    # Gestion des entrées utilisateur
    def input_box(self, titre='Question', message= ''):
        text, _ = QtWidgets.QInputDialog.getText(MainWindow, titre, message)
        return text
    def combo_box(self, titre='Selectionner', message="", list=[]):
        text, _ = QtWidgets.QInputDialog.getItem(MainWindow, titre, message, list)
        return text

    # Gestion du texte
    def convert_to_upper_case(self):
        """
        Converts the text in the QLineEdit to uppercase.
        """
        # Retrieve the current text in the QLineEdit
        current_text = self.nom.text()

        # Convert the text to uppercase
        upper_text = current_text.upper()

        # Set the converted text in the QLineEdit
        self.nom.setText(upper_text)
    def replace_accents(self, prenom=""):
        """
        Replaces accented characters in a given name with their non-accented counterparts.
        """
        accent_mapping  = {
            "à": "a",
            "á": "a",
            "â": "a",
            "ã": "a",
            "ä": "a",
            "å": "a",
            "æ": "ae",
            "ç": "c",
            "è": "e",
            "é": "e",
            "ê": "e",
            "ë": "e",
            "ì": "i",
            "í": "i",
            "î": "i",
            "ï": "i",
            "ð": "o",
            "ñ": "n",
            "ò": "o",
            "ó": "o",
            "ô": "o",
            "õ": "o",
            "ö": "o",
            "ù": "u",
            "ú": "u",
            "û": "u",
            "ü": "u",
            "ý": "y",
            "ÿ": "y"
        }

        result = ""
        for char in str(prenom).lower():
            if char in accent_mapping:
                result += accent_mapping [char]
            else:
                result += char
        return result.capitalize()
    def replace_special_characters(self):
        """
        Replaces specific special characters in the input text with predefined substitutions.
        """
        texte = self.scanner_input.text()
        texte = texte.replace("&", "1")
        texte = texte.replace("é", "2")
        texte = texte.replace('"', "3")
        texte = texte.replace("'", "4")
        texte = texte.replace("(", "5")
        texte = texte.replace("-", "6")
        texte = texte.replace("è", "7")
        texte = texte.replace("_", "8")
        texte = texte.replace("ç", "9")
        texte = texte.replace("à", "0")
        self.scanner_input.setText(texte)
    def hide_text(self):
        """
        Clears the label and stops the timer.
        """
        self.label.clear()
        self.timer.stop()


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    MainWindow.closeEvent = ui.closeEvent
    sys.exit(app.exec_())
